<?php

//
// includes

require_once('lib/helpers.php');


//
// enqueue assets

add_action('wp_enqueue_scripts', 'lc_enqueue_assets', 120);

function lc_enqueue_assets() {
	// get enqueue data
	$styles = lc_enqueue_helper("dist/css/main.css");
	$scripts = lc_enqueue_helper("dist/js/main.js");

	// dequeue child style, enque parent styles to allow overrides
	wp_dequeue_style('ebor-style');
	wp_enqueue_style('lc-parent', get_template_directory_uri() .'/style.css');

	// fonts
	wp_enqueue_style("lc-roboto", "https://fonts.googleapis.com/css?family=Roboto:200,400,400i,700", []);
	wp_enqueue_style("lc-fontawesome", "https://use.fontawesome.com/releases/v5.2.0/css/all.css", []);
	// re enque child style
  wp_enqueue_style('lc-main', $styles['uri'], [
		// 'lc-roboto',
		'lc-fontawesome',
		'lc-parent',
	], $styles['file_last_mod']);

	// enqueue scripts
  wp_enqueue_script("lc-main", $scripts['uri'], [
		"jquery",
	], $scripts['file_last_mod'], true);
}


//
// customize login page

add_action("login_enqueue_scripts", "lc_login_logo");

function lc_login_logo() { ?>
	<style type="text/css">
    <?php ob_start();
    include(locate_template('dist/css/login.css'));
    echo ob_get_clean(); ?>
  </style>
<?php }

add_filter("login_headerurl", "lc_login_logo_url");

function lc_login_logo_url() {
    return home_url();
}

add_filter("login_headertitle", "lc_login_logo_url_title");

function lc_login_logo_url_title() {
    return get_bloginfo("name");
}

add_action("widgets_init", "lc_widgets_init");

function lc_widgets_init() {

	/* template reference
	<?php if (is_active_sidebar("lc-custom-footer-1")) : ?>
	  <div>
	    <?php dynamic_sidebar("lc-custom-footer-1"); ?>
	  </div>
	<?php endif; ?>
	**/
  register_sidebar(array(
    "name" => __("Custom Footer 1", "lc"),
    "id" => "lc-custom-footer-1",
    "description" => __("Appears in the gray box above the footer", "lc"),
    "before_widget" => "",
		"after_widget"  => "",
		"before_title"  => "<h3 class='h1'>",
		"after_title"   => "</h3>",
  ));

	/* template reference
	<?php if (is_active_sidebar("lc-custom-footer-2")) : ?>
	  <div>
	    <?php dynamic_sidebar("lc-custom-footer-2"); ?>
	  </div>
	<?php endif; ?>
	**/
	register_sidebar(array(
    "name" => __("Custom Footer 2", "lc"),
    "id" => "lc-custom-footer-2",
    "description" => __("Appears in the blue box above the footer", "lc"),
		"before_widget" => "",
		"after_widget"  => "</h3>",
		"before_title"  => "<h3 class='h1 text-white text-center'>",
		"after_title"   => "</h3>",
  ));
}

add_action("customize_register", "lc_customize_register");

function lc_customize_register($wp_customize) {

	// group
	$wp_customize->add_panel('lc_footer', array(
		'priority'       => 500,
		'theme_supports' => '',
		'title'          => __('Footer Image and Map', 'lc'),
		'description'    => __('Set the footer image and map sections.', 'lc'),
	));

	// sections
	$wp_customize->add_section('lc_footer_image' , array(
		'title'    => __('Footer Image', 'lc'),
		'panel'    => 'lc_footer',
		'priority' => 10,
	));
	$wp_customize->add_section('lc_footer_map' , array(
		'title'    => __('Footer Map', 'lc'),
		'panel'    => 'lc_footer',
		'priority' => 20,
	));

	// Add settings
	$wp_customize->add_setting('lc_footer_image_block', array(
		'default'           => __('', 'lc'),
		'sanitize_callback' => 'sanitize_text'
	));
	$wp_customize->add_setting('lc_footer_map_block', array(
		'default'           => __('', 'lc'),
		'sanitize_callback' => 'sanitize_text'
	));

	// Add controls
	$wp_customize->add_control(new WP_Customize_Control(
	  $wp_customize,
		'lc_footer_image',
		  array(
        'label'    => __('Footer Image URL', 'lc'),
        'section'  => 'lc_footer_image',
				'settings' => 'lc_footer_image_block',
        'type'     => 'text'
	    )
	  )
	);
	$wp_customize->add_control(new WP_Customize_Control(
	  $wp_customize,
		'lc_footer_map',
		  array(
        'label'    => __('Footer Map URL', 'lc'),
        'section'  => 'lc_footer_map',
				'settings' => 'lc_footer_map_block',
        'type'     => 'text'
	    )
	  )
	);

	// callback
	function sanitize_text($text) {
	  return sanitize_text_field($text);
	}
}
