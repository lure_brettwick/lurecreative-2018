<section id="contact" class="section section-contact">

	<?php if (is_active_sidebar("lc-custom-footer-1")) : ?>

	  <section class="image-square left bg-gray-light">
	    <div class="col-md-6 image">
				<?php if (get_theme_mod('lc_footer_image_block') != '') : ?>
					<div class="background-image-holder fadeIn">
						<img src="<?php echo get_theme_mod('lc_footer_image_block'); ?>" alt="" />
					</div>
				<?php endif; ?>
	  		<!--
				<div class="background-image-holder fadeIn" style="background-image: url('/wp-content/uploads/2018/08/kc-skyline.jpg';); background-position: initial;">
	        <img width="881" height="668" src="/wp-content/uploads/2018/08/kc-skyline.jpg" class="background-image" alt="" srcset="https://lurecreative18.wpengine.com/wp-content/uploads/2018/08/kc-skyline.jpg 881w, https://lurecreative18.wpengine.com/wp-content/uploads/2018/08/kc-skyline-300x227.jpg 300w, https://lurecreative18.wpengine.com/wp-content/uploads/2018/08/kc-skyline-768x582.jpg 768w" sizes="(max-width: 881px) 100vw, 881px" style="display: none;">
	      </div>
				-->
	    </div>
	    <div class="col-md-6 col-md-offset-1 content">

	  		<div class="wpb_text_column wpb_content_element ">
	  			<div class="wpb_wrapper">

	  				<?php dynamic_sidebar("lc-custom-footer-1"); ?>

	  			</div>
	  		</div>
	  	</div>
	  </section>

	<?php endif; ?>
	<?php if (is_active_sidebar("lc-custom-footer-2")) : ?>

	  <section class="image-square right bg-blue text-white">
	    <div class="col-md-6 content">

	  		<div class="wpb_text_column wpb_content_element ">
	  			<div class="wpb_wrapper">

	  				<?php dynamic_sidebar("lc-custom-footer-2"); ?>

	  			</div>
	  		</div>
	  	</div>
	  	<div class="col-md-6 image">
	  		<div class="row fadeIn">

	  			<?php if (get_theme_mod('lc_footer_map_block') != '') : ?>
						<div class="embed-responsive" style="padding-bottom: 100%;">
							<iframe class="embed-responsive-item" src="<?php echo get_theme_mod('lc_footer_map_block'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
	  				</div>
					<?php endif; ?>

	      </div>
	    </div>
	  </section>

	<?php endif; ?>

</section>

<?php get_template_part('inc/content-footer', ebor_get_footer_layout()); ?>

</div><!--/body-wrapper-->

<?php get_template_part('inc/content-footer','modal');

global $foundry_modal_content;
echo do_shortcode($foundry_modal_content);

wp_footer(); ?>
</body>
</html>
