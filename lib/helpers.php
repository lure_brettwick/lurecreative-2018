<?php

// handle enqueue url and querystring to manage caching
function lc_enqueue_helper($file_path = null) {
	$file_path_uri = get_stylesheet_directory_uri() . '/' . $file_path;
	$file_last_mod = filemtime(get_stylesheet_directory() . '/' . $file_path);
	return array(
		'uri' => $file_path_uri,
		'file_last_mod' => $file_last_mod,
	);
}
