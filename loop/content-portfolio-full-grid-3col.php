<div class="col-md-4 col-sm-6 masonry-item project" data-filter="<?php echo ebor_the_terms('portfolio_category', ',', 'name'); ?>">
    <div class="image-tile hover-tile text-center">
				<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('grid', array('class' => 'background-image')); ?>
						<div class="image-tile-overlay">
								<?php
								$terms = get_the_terms(get_the_ID(), 'portfolio_category');
								$text_terms = '';

								foreach ($terms as $term) {
									if ($term->slug !== 'front-page') {
										$suffix = strlen($text_terms) ? ', ' : '';
										$text_terms = $suffix . $term->name;
									}
								}

								$text_terms = strlen($text_terms) ? '<br /><strong>' . $text_terms . '</strong>' : ''; ?>
								<?php the_title('<h4 style="font-size: 26px;" class="uppercase mb8">Case Study:' . $text_terms . '<br />', '</h4>'); ?>
						</div>
		        <div class="hover-state">
		            <a class="btn btn-lg btn-transparent uppercase" href="<?php the_permalink(); ?>">
										View Case Study
		            </a>
		        </div>
				</a>
    </div>
</div>
