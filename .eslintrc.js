module.exports = {
    "env": {
        "browser": true,
				"jquery": true
    },
		"globals": {
			"google": true
		},
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 5
    },
    "rules": {
        "indent": [
            "warn",
            "tab"
        ],
        "linebreak-style": [
            "warn",
            "unix"
        ],
				"no-unused-vars": [
						"warn"
				],
        "quotes": [
            "warn",
            "double"
        ],
        "semi": [
            "warn",
            "always"
        ]
    }
};
